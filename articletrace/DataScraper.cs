﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using OpenQA.Selenium;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Text.RegularExpressions;
using System.IO;

class UserInfo
{
    public string id { get; set; }
    public string phone { get; set; }
    public string address { get; set; }
}

class DataScraper : DriverController
{
    WebClient wc = new WebClient();

    public DataScraper()
    {
        CreatDriver(TYPE_DRIVER.TYPE_Chrome);
        driver.Navigate().GoToUrl("https://nid.naver.com/nidlogin.login?");

        wc.Headers.Clear();

        //wc.Headers.Add("Cache-Control", "no-cache, no-store, max-age=0, must-revalidate");
        wc.Headers.Add("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.105 Safari/537.36");
        wc.Headers.Add("Content-Type", "application/json, application/x-www-form-urlencoded, charset=UTF-8");
        wc.Headers.Add("Accept", "application/json, text/plain, */*");
        wc.Headers.Add("origin", "https://cafe.naver.com");
        //wc.Headers.Add("ajax-json", "true");

    }

    public void CollectCookie()
    {
        if (driver == null)
            return;
                
        var myCookies = driver.Manage().Cookies.AllCookies;
        List<string> listCookies = new List<string>();
        foreach (OpenQA.Selenium.Cookie itemCookie in myCookies)
        {
            listCookies.Add($"{itemCookie.Name}={itemCookie.Value}");
        }

        wc.Headers.Add(HttpRequestHeader.Cookie, string.Join(";" , listCookies));

        Thread.Sleep(100);
        //QuiteDriver();
    }

    public void PostComment(string id_ , string msg_)
    {
        using (Stream data = wc.OpenRead("https://apis.naver.com/cafe-home-web/cafe-home/v1/member/identifier"))
        {
            using (StreamReader reader = new StreamReader(data))
            {
                string s = reader.ReadToEnd();
                
                reader.Close();
                data.Close();

                var values = new System.Collections.Specialized.NameValueCollection();
                values.Add("content", msg_);
                values.Add("stickerId", "");
                values.Add("cafeId", Global.cafeid);
                values.Add("articleId", id_);
                values.Add("requestFrom", Global.requestFrom);

                var ret = wc.UploadValues("https://apis.naver.com/cafe-web/cafe-mobile/CommentPost.json", "POST", values);
                var retString = Encoding.Default.GetString(ret);


            }
        }
    }

    public string GetTotalArticle()
    {
        string url = "https://" +  $"cafe.naver.com/ArticleList.nhn?search.clubid={Global.cafeid}&search.boardtype=L";
        return wc.DownloadString(url);
    }
}

