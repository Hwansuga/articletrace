﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace articletrace
{
    public partial class Form1 : Form
    {
        BandDataScraper ds = null;


        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            ds = new BandDataScraper();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            ds.ProcessLogin();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            ds.CollectCookie();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            string ret = ds.GetBandInfo();
        }
    }
}
