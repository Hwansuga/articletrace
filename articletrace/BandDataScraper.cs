﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;


class BandDataScraper : DriverController
{
    WebClient wc = new WebClient();

    public BandDataScraper()
    {
        CreatDriver(TYPE_DRIVER.TYPE_Chrome);
        driver.Navigate().GoToUrl("https://nid.naver.com/nidlogin.login?");

        wc.Headers.Clear();

        //wc.Headers.Add("Cache-Control", "no-cache, no-store, max-age=0, must-revalidate");
        wc.Headers.Add("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.105 Safari/537.36");
        wc.Headers.Add("Content-Type", "application/json, application/x-www-form-urlencoded, charset=UTF-8");
        wc.Headers.Add("Accept", "application/json, charset=UTF-8");
        wc.Headers.Add("Host", "api.band.us");
        wc.Headers.Add("Referer", "https://band.us/band/" + Global.bandId);
        wc.Headers.Add("origin", "https://band.us");
        //wc.Headers.Add("ajax-json", "true");

    }

    public void ProcessLogin()
    {
        driver.Navigate().GoToUrl("https://band.us/home");

        Thread.Sleep(1000);
        driver.FindElementByXPath("//*[@id='header']/div/div/a[2]").Click();
        Thread.Sleep(1000);
        driver.FindElementByXPath("//*[@id='login_list']/li[3]/a").Click();
        Thread.Sleep(1500);
    }

    public void CollectCookie()
    {
        if (driver == null)
            return;

        var myCookies = driver.Manage().Cookies.AllCookies;
        List<string> listCookies = new List<string>();
        foreach (OpenQA.Selenium.Cookie itemCookie in myCookies)
        {
            listCookies.Add($"{itemCookie.Name}={itemCookie.Value}");
        }

        //listCookies.RemoveAt(listCookies.Count -1);

        wc.Headers.Add(HttpRequestHeader.Cookie, string.Join(";", listCookies));

        Thread.Sleep(100);
        //QuiteDriver();
    }

    public string GetBandInfo()
    {
        var Timestamp = new DateTimeOffset(DateTime.UtcNow).ToUnixTimeMilliseconds();
        string url = "https" + $"://api.band.us/v2.1.0/get_band_information?ts={Timestamp}&band_no={Global.bandId}";
        return wc.DownloadString(url);
    }
}

